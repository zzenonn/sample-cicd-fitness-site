from django.urls import path

from . import views as personal_views


urlpatterns = [
	path('answer/', personal_views.answer_survey, name='answer_survey'),	
	path('workoutlog/', personal_views.view_workout_log, name='workoutlog'),
]