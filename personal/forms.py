from django import forms

from .models import SurveyResponse, RESPONSES


class SurveyResponseForm(forms.ModelForm):
	answer = forms.ChoiceField(label='', choices=RESPONSES, widget=forms.RadioSelect())
	class Meta:
		model = SurveyResponse
		fields = ['answer']