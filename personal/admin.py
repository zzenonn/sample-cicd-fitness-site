from django.contrib import admin

from .models import SurveyItem, SurveyResponse, WorkoutLog


class SurveyResponseAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'user', 'question', 'answer')
	search_fields = ('user__user__username',)
	readonly_fields = ('user', 'question', )

	filter_horizontal = ()
	list_filter = ('question',)
	fieldsets = ()

class WorkoutLogAdmin(admin.ModelAdmin):
	list_display = ('user','date','workout')
	search_fields = ('user__user__username',)
	readonly_fields = ('user','date',)

	filter_horizontal = ()
	list_filter = ('workout',)
	fieldsets = ()

admin.site.register(SurveyItem)
admin.site.register(SurveyResponse, SurveyResponseAdmin)
admin.site.register(WorkoutLog, WorkoutLogAdmin)