from django.db import models

from user.models import Profile


class SurveyItem(models.Model):
	question = models.TextField()

	def __str__(self):
		return self.question


RESPONSES = (
		(True, 'Yes'),
		(False, 'No'),
	)
class SurveyResponse(models.Model):
	user = models.ForeignKey(Profile, related_name='responses', on_delete=models.CASCADE)
	question = models.ForeignKey(SurveyItem, related_name='responses', on_delete=models.CASCADE)
	answer = models.BooleanField(choices=RESPONSES)

	def __str__(self):
		return f'{self.user} response to {self.question}'

	class Meta:
		unique_together = (('user', 'question'),)

class WorkoutLog(models.Model):
	user = models.ForeignKey(Profile, related_name='wo_log', on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	workout = models.CharField(max_length=50, null=True, blank=True)

	def save(self, *args, **kwargs):
		if self.user.workout_category:
			self.workout = self.user.workout_category.category
			super(WorkoutLog, self).save(*args, **kwargs)
		else:
			pass
