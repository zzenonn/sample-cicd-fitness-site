from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test

from .forms import SurveyResponseForm
from .models import SurveyItem, SurveyResponse, WorkoutLog
from user.models import Profile


@login_required
def answer_survey(request):
	questions = SurveyItem.objects.all()
	user = request.user.profile
	responses = user.responses.all()
	SurveyResponseFormSet = inlineformset_factory(Profile, 
		SurveyResponse,
		can_delete=False, 
		form=SurveyResponseForm, 
		min_num=questions.count(), 
		extra=0)
	
	if request.method == 'POST':
		formset = SurveyResponseFormSet(request.POST, instance=user)
		if formset.is_valid():
			if responses:
				formset.save()
				messages.success(request, 'Answers Updated')
			else:
				instances = formset.save(commit=False)
				for instance, question in zip(instances, questions):
					instance.question = question
					instance.save()
				messages.success(request, 'Answers Submitted')
			user.save()
			return redirect('profile')
	else:
		formset = SurveyResponseFormSet(instance=user)
	
	context = {
		'formset':formset,
		'questions':questions,
		'responses':responses.all()
	}
	return render(request, 'personal/answer.html', context)

@login_required
@user_passes_test(lambda u: u.profile.workout_category, login_url='answer_survey')
def view_workout_log(request):
	user = request.user.profile
	if request.method == 'POST':
		WorkoutLog.objects.create(user=user)
		messages.success(request, 'New Workout Log Added')
		return redirect('workoutlog')

	workout_log = user.wo_log.all()

	context = {
		'workout_log':workout_log
	}
	return render(request, 'personal/workoutlog.html', context)