from django.contrib import admin

from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
	list_display = ('user', 'workout_category')
	search_fields = ('user__username',)
	readonly_fields = ('user', 'workout_category',)

	filter_horizontal = ()
	list_filter = ('workout_category',)
	fieldsets = ()

admin.site.register(Profile, ProfileAdmin)
