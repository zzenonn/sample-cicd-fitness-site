$(document).ready(function() {
	initialize();
	function initialize() {
		var songIndex = 0;
		var player = $('#player');
		var playlist = $('#playlist');
		var tracks = playlist.find('li a');
		var numSongs = tracks.length;

		//playSong(playlist.find('a')[0], player[0]);

		playlist.on('click', 'a', function(e) {
			e.preventDefault();
			link = $(this);
			songIndex = link.parent().index();
			playSong(link, player[0]);
		});

		player[0].addEventListener('ended', function(e) {
			var a;
			advance();
			a = playlist.find('a')[songIndex];
			//console.log(a);
			//console.log(songIndex);
			//console.log(numSongs);
			playSong(a, player[0]);
		});

		function advance() {
			songIndex++;
			if (songIndex >= numSongs) {
				songIndex = 0;
			}
		}
	}
	function playSong(link, audioplayer) {
		//console.log(link);
		audioplayer.src = $(link).find('source')[0].src;
		par = $(link).parent();
		par.addClass('active').siblings().removeClass('active');
		audioplayer.load();
		audioplayer.play();
	}
});