from django.urls import path
from django.contrib.auth.decorators import login_required

from . import views as user_views


urlpatterns = [
	path('', user_views.home,name='home'),
	path('register/', user_views.register,name='register'),
	path('login/', user_views.CustomLoginView.as_view(), name='login'),
    path('logout/', login_required(user_views.CustomLogoutView.as_view(), login_url='home'), name='logout'),
    path('profile/', login_required(user_views.profile), name='profile'),
]