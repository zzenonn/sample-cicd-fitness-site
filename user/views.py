from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, views
from django.contrib.auth.decorators import user_passes_test
from django.contrib.messages.views import SuccessMessageMixin

from .forms import UserRegisterForm, UserUpdateForm, AuthForm
from .models import Profile
from music.models import Playlist


def home(request):
	return render(request, 'user/home.html')

@user_passes_test(lambda u: not u.is_authenticated, login_url='home')
def register(request):
	if request.method =='POST':
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			form.save()
			messages.success(request, 'Account Created')
			new_user = authenticate(
				username=form.cleaned_data['username'],
				password=form.cleaned_data['password1']
			)
			login(request, new_user)
			Profile.objects.create(user=new_user)
			Playlist.objects.create(user=new_user.profile)
			return redirect('home')
	else:
		form = UserRegisterForm()
	return render(request, 'user/register.html',{'form':form})

def profile(request):
	if request.method =='POST':
		form = UserUpdateForm(request.POST, instance=request.user)
		if form.is_valid():
			form.save()
			messages.success(request, 'Account Updated')
			return redirect('profile')
	else:
		form = UserUpdateForm(instance=request.user)

	profile = request.user.profile
	responses = profile.responses.all()
	if profile.workout_category:
		routines = profile.workout_category.routines.all()
	else:
		routines = None
	songs = profile.playlist.songs.all()
	
	context = {
		'form':form,
		'responses':responses,
		'routines':routines,
		'songs':songs
	}
	return render(request, 'user/profile.html', context)

class CustomLoginView(SuccessMessageMixin, views.LoginView):
	redirect_authenticated_user = True
	authentication_form = AuthForm
	template_name = 'user/login.html'
	success_message = "Log In Success"

class CustomLogoutView(views.LogoutView):
	next_page = 'login'
	def dispatch(self, request, *args, **kwargs):
	    response = super().dispatch(request, *args, **kwargs)
	    messages.add_message(request, messages.INFO, 'Log Out Success')
	    return response
