from django.db import models
from django.contrib.auth.models import User

from workout.models import WorkoutCategory


class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	workout_category = models.ForeignKey(WorkoutCategory, 
		related_name='profiles', 
		null=True, 
		blank=True, 
		on_delete=models.SET_NULL)



	def save(self, *args, **kwargs):
		total = self.responses.all().count()
		if total > 0:
			total_yes = self.responses.filter(answer=True).count()
			if total_yes == total:
				self.workout_category = WorkoutCategory.objects.get(category='High')
			elif total_yes > (total // 2):
				self.workout_category = WorkoutCategory.objects.get(category='Medium')
			else:
				self.workout_category = WorkoutCategory.objects.get(category='Low')
		else:
			self.workout_category = None
		super(Profile, self).save(*args, **kwargs)


	def __str__(self):
		return f'{self.user.username}'
