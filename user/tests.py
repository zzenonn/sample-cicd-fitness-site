from django.test import TestCase
from django.urls import resolve

from . import views as user_views

class GetPageTest(TestCase):

    def test_home_page(self):
        found = resolve('/')
        self.assertEqual(found.func, user_views.home)

    def test_login_page(self):
        found = resolve('/login/')
        self.assertEqual(found.func.view_class, user_views.CustomLoginView)

    def test_registration_page(self):
        found = resolve('/register/')
        self.assertEqual(found.func, user_views.register)
