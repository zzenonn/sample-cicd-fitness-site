from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


class UserRegisterForm(UserCreationForm):
	username = forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Username'}))
	first_name = forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
	last_name = forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
	email = forms.EmailField(label='',widget=forms.EmailInput(attrs={'placeholder': 'Email'}))
	password1 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
	password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password'}))
	
	class Meta:
		model = User
		fields = ['username', 'first_name', 'last_name', 'email','password1','password2']

class AuthForm(AuthenticationForm):
	username = forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Username'}))
	password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))

class UserUpdateForm(forms.ModelForm):
	username = forms.CharField()
	first_name = forms.CharField()
	last_name = forms.CharField()
	email = forms.EmailField()
	
	class Meta:
		model = User
		fields = ['username', 'first_name', 'last_name', 'email']