from django.urls import path

from . import views as nutrition_views


urlpatterns = [
	path('nutrition_log/', nutrition_views.nutrition_log, name='nutrition_log'),
]