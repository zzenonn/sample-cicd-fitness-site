from django.contrib import admin

from .models import NutritionLog

class NutritionLogAdmin(admin.ModelAdmin):
	list_display = ('user', 'date', 'calorie_intake', 'carbohydrates', 'protein', 'fat')
	search_fields = ('user__user__username',)
	readonly_fields = ('user','date',)

	filter_horizontal = ()
	list_filter = ()
	fieldsets = ()

admin.site.register(NutritionLog, NutritionLogAdmin)
