from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .forms import NutritionLogForm


@login_required
def nutrition_log(request):
	user = request.user.profile
	if request.method =='POST':
		form = NutritionLogForm(request.POST)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.user = user
			instance.save()
			messages.success(request, 'New Nutrition Log Added')
			return redirect('nutrition_log')
	else:
		form = NutritionLogForm()

	nutrition_logs = user.n_log.all()

	context = {
		'form': form,
		'nutrition_logs': nutrition_logs
	}
	return render(request, 'nutrition/nutrition_log.html', context)