from django.db import models

from user.models import Profile


class NutritionLog(models.Model):
	user = models.ForeignKey(Profile, related_name='n_log', null=True, blank=True, on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	calorie_intake = models.IntegerField(blank=True, null=True)
	carbohydrates = models.IntegerField(blank=True, null=True)
	protein = models.IntegerField(blank=True, null=True)
	fat = models.IntegerField(blank=True, null=True)
