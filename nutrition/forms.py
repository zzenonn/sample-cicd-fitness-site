from django import forms

from .models import NutritionLog


class NutritionLogForm(forms.ModelForm):
	class Meta:
		model = NutritionLog
		fields = ['calorie_intake',	'carbohydrates', 'protein', 'fat']