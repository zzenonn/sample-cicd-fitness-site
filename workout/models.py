from django.db import models


class WorkoutCategory(models.Model):
	category = models.CharField(max_length=50)

	def __str__(self):
		return f'{self.category}'

class WorkoutRoutine(models.Model):
	category = models.ForeignKey(WorkoutCategory, related_name='routines', on_delete=models.CASCADE)
	routine = models.CharField(max_length=50)

	def __str__(self):
		return f'{self.routine} ({self.category})'