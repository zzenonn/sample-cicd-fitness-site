from django.contrib import admin

from .models import WorkoutCategory, WorkoutRoutine


class WorkoutRoutineAdmin(admin.ModelAdmin):
	list_display = ('__str__', 'routine','category',)
	search_fields = ()
	readonly_fields = ()

	filter_horizontal = ()
	list_filter = ('category',)
	fieldsets = ()

admin.site.register(WorkoutCategory)
admin.site.register(WorkoutRoutine, WorkoutRoutineAdmin)
