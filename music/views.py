from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .forms import PlaylistSongForm


@login_required
def select_playlist_song(request):
	playlist = request.user.profile.playlist
	if request.method =='POST':
		form = PlaylistSongForm(request.POST, instance=playlist)
		if form.is_valid():
			form.save()
			messages.success(request, 'Playlist Updated')
			return redirect('profile')
	else:
		form = PlaylistSongForm(instance=playlist)

	context = {
		'form':form
	}
	return render(request, 'music/add_songs.html', context)