from django.db import models

from user.models import Profile


class Song(models.Model):
	title = models.CharField(max_length=50, null=True, blank=True)
	artist = models.CharField(max_length=20, null=True,blank=True)
	file = models.FileField(null=True,blank=True)

	def __str__(self):
		return f"{self.title} by {self.artist}"

class Playlist(models.Model):
	user = models.OneToOneField(Profile, on_delete=models.CASCADE)
	songs = models.ManyToManyField(Song, related_name='playlists', blank=True)

	def __str__(self):
		return f"{self.user.user.username}'s Playlist"