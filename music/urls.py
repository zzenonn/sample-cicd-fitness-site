from django.urls import path

from . import views as music_views


urlpatterns = [
	path('add_songs/', music_views.select_playlist_song, name='playlist_song'),
]