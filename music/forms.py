from django import forms

from .models import Playlist, Song


class PlaylistSongForm(forms.ModelForm):
	songs = forms.ModelMultipleChoiceField(
        queryset=Song.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        blank=True
    )
	class Meta:
		model = Playlist
		fields = ['songs']